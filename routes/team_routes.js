const express = require("express")
const router = express.Router()
const mongoose = require("mongoose")

const Team = require("../models/Team.js")

// Excercise 
// Create a team
router.post("/", (req, res) =>{
	let newTeam = new Team({
		name : req.body.name
	});
	newTeam.save((saveErr, newTeam) =>{
		
		if(saveErr) return console.log(saveErr)
	res.send("Team created")
})
})

// 1. Retrieve all the teams
router.get("/", (req, res)=>{
	Team.find().then(teams =>{
		res.send(teams)
	})
})

// // 3. Retrieve a single team
router.get("/:id", (req, res)=>{
	// res.send("Looking for user with id = " + req.params.id)
	Team.findById(req.params.id, (err, team) =>{
		if(err) return console.log(err)
			res.send(team);
	})
})

// // 4.Delete a team
router.delete("/:id", (req,res)  =>{
	Team.findByIdAndDelete(req.params.id, (err, team) =>{
		if (err){
			res.send("Id is not found");
		}else{
			return res.send("Team deleted");
		}

})
})
module.exports = router