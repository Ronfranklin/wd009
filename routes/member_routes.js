// require express

const express = require("express")
const router = express.Router() // we use express' routing tp create the routes
const mongoose = require("mongoose")

// get the member module
const Member = require("../models/Member.js")

// Actual routes
router.get("/", (req, res)=>{
	Member.find().then(members =>{
		res.send(members)
	})
})

// CRUD in Express
// saving in the database
router.post("/", (req, res) =>{
	let newMember = new Member({
		firstName : req.body.firstName,
		lastName: req.body.lastName,
		position: req.body.position
	});
	newMember.save((saveErr, newMember) =>{
		// if an error was encountered while saving the document, output will error in the console
		// note that saveErr is user-defined
		if(saveErr) return console.log(saveErr)
		// if successful, return a response saying member created
	res.send("Member created")
})
})


// Retrieve a single member
router.get("/:id", (req, res)=>{
	// res.send("Looking for user with id = " + req.params.id)
	Member.findById(req.params.id, (err, member) =>{
		if(err) return console.log(err)
			res.send(member);
	})
})

// Update singe user
router.put("/:id", (req, res)=>{
	//the params determines which entry to update
	//the body determines what values to update in the entry
	Member.findByIdAndUpdate(req.params.id, {
		$set:{
			firstName: req.body.firstName,
			lastName: req.body.lastName,
			posistion: req.body.posistion
		}
	}, (err) =>{
		if(err){
			res.send("NO ID found")
		}else{
			res.send("User Updated")
		}
	})
})

// delete single User
router.delete("/:id", (req,res)  =>{
	// fill this out
	// if id is not found, return the statement "NO user found"
	Member.findByIdAndDelete(req.params.id, (err, member) =>{
		if (err){
			res.send("Id is not found");
		}else{
			return res.send("User deleted");
		}

	// if id is found, delete the document and return "user deleted"
	// Q: How to delete using mongoose 
})
})

module.exports = router