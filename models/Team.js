const mongoose = require("mongoose");

const Schema = mongoose.Schema //use mongoose's Schema as our basis

// Actual Schema
const teamSchema = new Schema({
	name: String},
	{
		timestamps: true
});

//export the model as a module
// mongoose.model("Model Name", "Model Schema")
module.exports = mongoose.model("Team", teamSchema);
//this create a model calles "Task" using the schema "taskSchma" and exports it to be used by index.js
