const mongoose = require("mongoose");

const Schema = mongoose.Schema //use mongoose's Schema as our basis

// Actual Schema
const taskSchema = new Schema({
	teamId: String,
	description: String,
	isCompleted: Boolean },
	{
		timestamps: true
});

//export the model as a module
module.exports = mongoose.model("Task", taskSchema);
//this create a model calles "Task" using the schema "taskSchma" and exports it to be used by index.js
