const mongoose = require("mongoose");
const Schema = mongoose.Schema //use mongoose's Schema as our basis

// Actual Schema
const memberSchema = new Schema({
	firstName: {
		type:String, 
		require:true
	},
	lastName: {
		type:String, 
		require:true
	},
	position: {
		type:String, 
		require:true
	}
	},

	{
		timestamps: true
	});

//export the model as a module
// mongoose.model("Model Name", "Model Schema")
module.exports = mongoose.model("Member", memberSchema);
//this create a model calles "Task" using the schema "taskSchma" and exports it to be used by index.js
