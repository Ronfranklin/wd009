// Declare dependencies
const express = require('express');

//intialize express app
const app = express();
const mongoose = require('mongoose');
// const port = 3001;

// Database connection
// Local
// mongoose.connect("mongodb://localhost:27017/merng_tracker", {useNewUrlParser:true});

// import the models
const Member = require("./models/Member.js");
// use express' json body request handler
app.use(express.json());
// app.use(express.json());->allows the application to read JSON data in the body of the request

const Team = require("./models/Team.js");
const Task = require("./models/Task.js");


// Online
mongoose.connect("mongodb+srv://Ronfranklin:ronfranklin@nosqlsession-1kxtd.mongodb.net/b43_merng_db?retryWrites=true&w=majority", {useNewUrlParser:true})
// check if the connection is succeeds
mongoose.connection.once("open", ()=>{
	console.log("now connected to the MongoDB server")
})

// server intialization

// =================================================================================================

// include the member_routes.js
let member_route =require("./routes/member_routes.js")
app.use("/members", member_route)

// include the team_routes.js
let team_route = require("./routes/team_routes.js")
app.use("/teams", team_route)


// import the instantiation of the apollo server
const server = require("./queries/queries.js");

// the app will be served by the apollo server instead of express

server.applyMiddleware({
	app
})

// server initialization
app.listen(3000, ()=>{
	 console.log(`🚀  Server ready at http://localhost:3000${server.graphqlPath}`);
})
