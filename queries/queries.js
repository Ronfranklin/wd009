const {ApolloServer, gql} = require("apollo-server-express");

// mongoose models
const Member = require('../models/Member.js');
const Task = require('../models/Task.js');
const Team = require('../models/Team.js');

// CRUD
// type Mutation == Create / Update / Delete

const typeDefs = gql`

		#type Query == Retrieve/Read
	# this is a comment
	# the type Query is the root of all GraphQL queries
	# this is used for executing "GET" request

	type TeamType{
		id : ID
		name : String
	}

	type TaskType{
		id: ID
		description: String
		teamId : String
		isCompleted : Boolean
	}

	type MemberType{
		id: ID
		firstName : String!
		lastName : String!
		position :String!
	}

	type Query{
		#create a query called hello that will expect a string data type
		hello : String!
		getTeams : [TeamType]
		getTasks : [TaskType]
		getMembers : [MemberType]
		getMember (id: ID) : MemberType
		getTeam(id: ID!) : TeamType
		getTask(id: ID!) :TaskType
		
	}

	#CUD Functionality
	#We are mutating the server/database

	type Mutation{
		createTeam(
			name: String!
		) : TeamType

		createTask(
			teamId: String! 
			description: String!
			isCompleted: Boolean!
		) : TaskType

		createMember(
			firstName: String!
			lastName: String!
			position: String!
		) : MemberType
#================================
		updateTeam(
			id : ID!,
			name: String!
		): TeamType

		updateTask(
			id: ID,
			teamId: String,
			description: String,
			isCompleted: Boolean
		):TaskType

		updateMember(
			firstName: String!,
			lastName: String!,
			position: String!
		):MemberType
#==================================
		deleteTeam(
			id: ID!,
			name : String
		): TeamType

		deleteTask(
			teamId: String!,
			description : String!,
			isCompleted : Boolean
		):TaskType

		deleteMember(
			firstName: String!,
			lastName: String!,
			position: String
		):MemberType
	}

	#required to have semi colon at the end of the code
`;

const resolvers = {
	// what are we going to do when the query is executed
	Query : {
		hello : () => "My First Query",
		getTeams : ()=> {
			return Team.find({})
		},
		getTasks : () => {
			return Task.find({})
		},
		getMembers: () =>{
			return Member.find({})
		},
		getMember: (_, args) =>{
			console.log("Nag execute ka")
			console.log(args.id)
			return Member.findById(args.id)
		},
		getTeam: (_, args) =>{
			console.log(args)
			console.log("Nag getTeam Query")
			return Team.findById(args.id)
		},
		getTask: (_, args) =>{
			console.log(args)
			console.log("You are on the Task model")
			return Task.findOne({_id: args.id})
		}
	},
// =============================================
	Mutation: {
		createTeam : (_,args) =>{
			console.log(args) 

			let newTeam = Team({
				name  : args.name
			})
			console.log(newTeam)

			return newTeam.save()
		},
		
		createTask: (_,args) =>{
			console.log(args)

			let bagongTask = Task({
				teamId : args.teamId,
				description : args.description,
				isCompleted : args.isCompleted
			})
			console.log(bagongTask)

			return bagongTask.save()
		},

		createMember: (_,args) =>{
			console.log(args)

			let BagongMember = Member({
				firstName : args.firstName,
				lastName : args.lastName,
				position : args.position
			})
			console.log(BagongMember)

			return BagongMember.save()
		},
// ===========================================
		updateTeam: (_,args) =>{
			// finOneAndUpdate(condition, update, callback function)
			console.log(args)
			return Team.findOneAndUpdate(args.id, {
				$set:{
					name : args.name,
				}
			})
		},

		updateTask: (_,args) =>{
			console.log(args)
			return Task.findOneAndUpdate(args.id, {
				$set:{
					teamId: args.teamId,
					description: args.description,
					isCompleted: args.isCompleted
				}
			})
		},

		updateMember: (_,args) =>{
			console.log(args)
			return Member.finOneAndUpdate(args.id,{
				$set:{
					firstName: args.firstName,
					lastName: args.lastName,
					position: args.position
				}
			})
		},
// ============================================
		deleteTeam: (_,args) =>{
			console.log(args)
			return Team.findByIdAndDelete(args.id)
		},

		deleteTask: (_,args) =>{
			console.log(args)
			return Task.findByIdAndDelete(args.id)
		},

		deleteMember: (_,args)=>{
			console.log(args)
			return Member.findByIdAndDelete(args.id)
		}
	}
}


// creat an instance of the apollo server
// in the most basic sense, The ApolloServer can be started by passing schema type definitions
// (TypeDefs) and the resolver responsible for fetching the data for the declared request/quesries

const server = new ApolloServer({
	typeDefs,
	resolvers
})

module.exports = server;